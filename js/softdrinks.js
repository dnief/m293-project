//JSON direkt hier hineinkopiert, weil import und fetch nicht funktionieren
const jsonData = [
    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "https://theoriginalmaidsofhonour.co.uk/cdn/shop/products/CokeCan_1334x.png?v=1594893838",
        "volume": "50cl"
    },
    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "https://bonvila.com/cdn/shop/products/Canoffanta.jpg?v=1600959519",
        "volume": "50cl"
    },
    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "https://m.media-amazon.com/images/I/61Q7SR0r9XL._SL1500_.jpg",
        "volume": "50cl"
    },
    {
        "name": "Red bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "https://molloys.ie/cdn/shop/products/redbull_600x.jpg?v=1612879920",
        "volume": "50cl"
    }
]
console.log(jsonData);
populateGallery(jsonData);


// Gallerie ausfüllen mit Items aus jsonData
function populateGallery(data) {
    const softGallery = document.getElementById("softGallery");
    
    // Initierung eines leeren Strings für die Items
    let galleryHTML = '';
    
    // Für jedes Item das HTML-Template ausfüllen
    data.forEach(itemData => {
        galleryHTML += `
            <div class="softItem">
                <img class="softImg" src="${itemData.imageUrl}" alt="${itemData.name}">
                <p class="softName">${itemData.name}</p>
                <div class="softCustomize">
                    <select>
                        <option value="50cl">50cl</option>
                        <option value="100cl">100cl</option>   
                    </select> 
                    <div class="softPrice">
                        ${itemData.prize} <button class="addToCart" type="button">
                            <img src="images/shoppingcart.png" alt="Warenkorb">
                        </button>
                    </div>
                </div>
            </div>
        `;
    });

    // Das ausgefüllte Template in die HTML-Gallerie einfügen
    softGallery.innerHTML = galleryHTML;
}

