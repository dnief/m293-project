//JSON direkt hier hineinkopiert, weil import und fetch nicht funktionieren
const jsonData = [
    {
        "name": "Green salad with tomatoes",
        "prize": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        "imageUrl": "https://makingsalads.com/wp-content/uploads/2022/01/cucumber-lettuce-tomato-salad.jpg"
    },
    {
        "name": "Tomato salad with mozzarella",
        "prize": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            " Mozzarella"
        ],
        "imageUrl": "https://i2.wp.com/www.downshiftology.com/wp-content/uploads/2019/07/Caprese-Salad-3-3.jpg"
    },
    {
        "name": "Field salad with egg",
        "prize": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            " Egg"
        ],
        "imageUrl": "https://images.ctfassets.net/puwhzcrt0wgo/236rFUcs1IbpjW7iCpclpy/a7e862fe2a8ee37dba400ef169cee024/field_greens_with_potatoes_eggs_and_shiitake_mushrooms_1712x1126.jpg"
    },
    {
        "name": "Rocket with parmesan",
        "prize": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            " Parmesan"
        ],
        "imageUrl": "https://img.taste.com.au/2h3qjyGz/taste/2016/11/rocket-tomato-and-parmesan-salad-75324-1.jpeg"
    }
]
console.log(jsonData);
populateGallery(jsonData);

// Gallerie ausfüllen mit Items aus jsonData
function populateGallery(data) {
    const saladGallery = document.getElementById("saladGallery");
    
    // Initierung eines leeren Strings für die Items
    let galleryHTML = '';
    
    // Für jedes Item das HTML-Template ausfüllen
    data.forEach(itemData => {
        galleryHTML += `
            <div class="saladItem">
                    <img class="saladImg" src="${itemData.imageUrl}" alt="${itemData.name}">
                    <p class="saladName">${itemData.name}</p>
                    <p class="saladDesc">${itemData.ingredients}</p>
                    <div class="saladCustomize">
                        <select>
                            <option value="italian_dressing">Italian dressing</option>
                            <option value="french_dressing">French dressing</option>
                            <option value="no_dressing">No dressing</option>
                        </select> 
                        <div class="saladPrice"> 
                            ${itemData.prize} <button class="addToCart" type="button">
                                <img src="images/shoppingcart.png" alt="Warenkorb">
                            </button>
                        </div>
                    </div>
                </div>
        `;
    });

    // Das ausgefüllte Template in die HTML-Gallerie einfügen
    saladGallery.innerHTML = galleryHTML;
}