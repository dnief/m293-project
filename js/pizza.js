//import und fetch werden nicht con CORS zugelassen (json-file gelöscht)
//import jsonData from 'json/pizzas.json' assert {type:"json"};

//Darum habe ich das JSON direkt hier hineinkopiert
const jsonData = [
    {
        "name": "Piccante",
        "prize": "16$",
        "id": 1,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Spicy Salami",
            " Chilies",
            " Oregano"
        ],
        "imageUrl": "https://www.oilvinegar.at/media/recipe/pizza_piccante.jpg"
    },
    {
        "name": "Giardino",
        "prize": "14$",
        "id": 2,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Artichokes",
            " Fresh Mushrooms"
        ],
        "imageUrl": "https://pizzeriagiardino.ch/data/Ressources/1474898730-5DSR5985.jpg"
    },
    {
        "name": "Prosciuotto e funghi",
        "prize": "15$",
        "id": 3,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Ham",
            " Fresh Mushrooms",
            " Oregano"
        ],
        "imageUrl": "https://i1.wp.com/www.piccolericette.net/piccolericette/wp-content/uploads/2019/10/4102_Pizza.jpg?resize=895%2C616&ssl=1"
    },
    {
        "name": "Quattro formaggi",
        "prize": "13$",
        "id": 4,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Parmesan",
            " Gorgonzola"
        ],
        "imageUrl": "https://cdn.gutekueche.de/upload/rezept/21247/quattro-formaggi.jpg"
    },
    {
        "name": "Quattro stagioni",
        "prize": "17$",
        "id": 5,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Ham",
            " Artichokes",
            " Fresh Mushrooms"
        ],
        "imageUrl": "https://farm5.staticflickr.com/4078/4932649252_b0aaa733ae.jpg"
    },
    {
        "name": "Stromboli",
        "prize": "12$",
        "id": 6,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Fresh Chilies",
            " Olives",
            " Oregano"
        ],
        "imageUrl": "https://www.sanpellegrino.com/uk/sites/g/files/xknfdk2256/files/styles/recipe_fallback/public/Italian_Stromboli_post.jpg?itok=vfBrJSwu"
    },
    {
        "name": "Verde",
        "prize": "13$",
        "id": 7,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Broccoli",
            " Spinach",
            " Oregano"
        ],
        "imageUrl": "https://www.boodschappen.nl/app/uploads/recipe_images/4by3_header@2x/ultima-pizza-verde.jpg"
    },
    {
        "name": "Rustica",
        "prize": "15$",
        "id": 8,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Ham",
            " Bacon",
            " Onions",
            " Garlic",
            " Oregano"
        ],
        "imageUrl": "https://www.foodnetwork.com/content/dam/images/food/fullset/2018/7/24/0/FN_Pizza-Rustica-V2-H_s4x3.jpg"
    }
]
console.log(jsonData);
populateGallery(jsonData);


// Gallerie ausfüllen mit Items aus jsonData
function populateGallery(data) {
    const pizzaGallery = document.getElementById("pizzaGallery");
    
    // Initierung eines leeren Strings für die Items
    let galleryHTML = '';
    
    // Für jedes Item das HTML-Template ausfüllen
    data.forEach(itemData => {
        galleryHTML += `
            <div class="pizzaItem">
                    <img class="pizzaImage" src="${itemData.imageUrl}" alt="${itemData.name}">
                    <div class="pizzaName">
                        <p>${itemData.name}</p>
                        <div class="pizzaPrice">
                            <p>${itemData.prize}</p>
                            <button class="addToCart" type="button">
                                <img src="images/shoppingcart.png" alt="Warenkorb">
                            </button>
                        </div>
                    </div>
                    <p>${itemData.ingredients}</p>
                </div>
        `;
    });
        
    // Das ausgefüllte Template in die HTML-Gallerie einfügen
    pizzaGallery.innerHTML = galleryHTML;
}

//JSON fetch geht auch nicht...
// fetch('softdrinks.json')
//     .then(response => response.json())
//     .then(data => {
//         // Once the data is fetched and parsed, you can populate the gallery
//         console.log(data);
//         //populateGallery(data);
//     })
//     .catch(error => {
//         console.error('Error fetching JSON data:', error);
//     });
