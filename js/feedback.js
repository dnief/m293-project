function getFormValues() {

    let form = document.getElementById("feedbackForm");

    //Felder festhalten
    let pizzarating = form.querySelector('input[name="pizza"]:checked + label').textContent;
    let pricerating = form.querySelector('input[name="price"]:checked + label').textContent;
    let name = form.elements["name"].value;
    let email = form.elements["email"].value;
    let improvements = form.elements["improvements"].value;

    //gespeicherte Daten können im Log angezeigt werden, entfernt durch Navigation zur Danke-Seite
    console.log("Pizza Rating: " + pizzarating);
    console.log("Price Rating: " + pricerating);
    console.log("Name: " + name);
    console.log("Email: " + email);
    console.log("Improvements: " + improvements);

    //Zur Danke-Seite navigieren
    window.location.href = "thanks.html";
    return false;
}

