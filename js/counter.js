//Ausführen, wenn Seite geladen wird
document.addEventListener("DOMContentLoaded", function() {
    // counter mit lokalem Speicher oder 0 initialisieren
    let counter = parseInt(localStorage.getItem("counter")) || 0;

    //Button onclicks EventListener hinzufügen
    const addToCartButtons = document.querySelectorAll('.addToCart');
    addToCartButtons.forEach(button => {
        button.addEventListener('click', addToCart);
    });

    //Reset EventListener und Funktion
    const resetButtons = document.getElementById('resetCart');
    resetButtons.addEventListener('click', function() {
        if (counter > 0) {
            alert("Thank you for your order!");
        }
        counter = 0;
        updateCounterDisplay(counter);
        localStorage.setItem("counter", 0);
    });

    updateCounterDisplay(counter);

    //Counter erhöhen, im lokalen Speicher ablegen
    function addToCart() {
        counter++;
        updateCounterDisplay(counter);
        document.getElementById("counter").innerHTML = counter.toString();
        localStorage.setItem("counter", counter.toString());
    }
    //aktueller Count auf Seite innerHTML darstellen
    function updateCounterDisplay(counterValue) {
        document.getElementById("counter").innerHTML = counterValue.toString();
    }
});





